import datetime

from django.test import TestCase
from django.utils import timezone

from .models import Question
from django.urls import reverse

# Questionモデルに関するユニットテスト
class QuestionModelTests(TestCase):
    def test_was_published_recently_with_future_published_question(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is in the future
        """
        future = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=future)
        self.assertIs(future_question.was_published_recently(), False)


    def test_was_published_recently_with_old_question(self):
        """
        was_published_recently() return False for questions whose pub_date
        is older than 1 day
        """
        past = timezone.now() - datetime.timedelta(days=1, seconds=1)
        past_question = Question(pub_date=past)
        self.assertIs(past_question.was_published_recently(), False)


    def test_was_published_recently_with_recent_question(self):
        """
        was_published_recently() return True for questions whose pub_date
        is within the last day
        """
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question  = Question(pub_date=time)
        self.assertIs(recent_question.was_published_recently(), True)

# question_textとdaysを受け取ってQuestionを作成する関数
def create_question(question_text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)

# QuestionIndexViewに関するテスト（画面遷移テスト）
class QuestionIndexViewTests(TestCase):
    def test_no_questions(self):
        """
        If no questions exist, an appropriate message is displayed
        テストがなかったら存在しないとresponseが伝えてくれる
        """
        response = self.client.get(reverse("polls:index"))
        # 正常にresponseが返されることの確認
        self.assertEqual(response.status_code, 200)
        # responseの中に"No polls available"があることの確認
        self.assertContains(response, "No polls are available.")
        # responseのquerysetが空であることの確認
        self.assertQuerysetEqual(response.context["latest_question_list"], [])

    def test_past_question(self):
        """
        Questions with a pub_date in the past are displayed on the index page
        過去の質問はviewにディスプレイされることの確認
        """
        create_question(question_text="Past Question", days=-30)
        response = self.client.get(reverse("polls:index"))
        # querysetがpast questionのみ入っていることの確認
        self.assertQuerysetEqual(response.context["latest_question_list"],
            ["<Question: Past Question>"])

    def test_future_question(self):
        """
        Question with a pub_date in the future are not displayed on the index page
        未来の質問はディスプレイされないことの確認
        追加でNo polls are availableがつくことも確認
        """
        create_question(question_text="Future Question", days=30)
        response = self.client.get(reverse("polls:index"))
        self.assertContains(response, "No polls are available.")
        # querysetが空リストであることの確認
        self.assertQuerysetEqual(response.context["latest_question_list"], [])

    def test_future_and_past_question(self):
        """
        Even if both past and future questions exist, only past questions
        are displayed
        過去と未来の質問があったときに過去の質問のみディスプレイする
        """
        create_question(question_text="Past Question", days=-30)
        create_question(question_text="Future Question", days=30)
        response = self.client.get(reverse("polls:index"))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context["latest_question_list"],
            ["<Question: Past Question>"])

    def test_two_past_question(self):
        """
        The questions index page may display multiple questions
        indexページは複数のquestionを表示できるか確認
        """
        create_question(question_text="Past Question 1", days=-30)
        create_question(question_text="Past Question 2", days=-5)
        response = self.client.get(reverse("polls:index"))
        # 新しい順に並んでいることも確認する（2 -> 1）
        self.assertQuerysetEqual(response.context["latest_question_list"],
                ["<Question: Past Question 2>", "<Question: Past Question 1>"])

# QuestionDetailsViewに関するテスト
class QuestionDetailViewTests(TestCase):
    def test_future_question(self):
        """
        The details view of a question with a pub_date in the future
        returns a 404 not found
        未来の質問のurlを開いたときに404を返すことを確認
        """
        future_question = create_question(question_text="Future Question", days=30)
        url = reverse("polls:detail", args=(future_question.id,))
        response = self.client.get(url)
        # 404か確認
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        """
        the detail view of a question with a pub_date in the past
        displays the question text
        過去の質問に対しては存在すること、テキストが表示されることを確認する
        """
        past_question = create_question(question_text="Past Question.", days=-5)
        url = reverse("polls:detail", args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)

# テスト作成のためのファイル
# Create your tests here.
