# Register your models here.
from django.contrib import admin
from .models import Choice, Question

# フィールドの表示順を変更するためにclassを作成する
class QuestionAdmin(admin.ModelAdmin):
    fields = ["pub_date", "question_text"]

# 複数のフィールドに分割するときはタプルで設定する
class QuestionAdminMultipleSets(admin.ModelAdmin):
    fieldsets = [
        (None,                  {"fields": ["question_text"]}),
        ("Date information",    {"fields": ["pub_date"]}),
    ]

# Choiceオブジェクトを同時に追加できるようなQuestion Form
class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 3


class QuestionAdminWithChoice(admin.ModelAdmin):
    # list_displayで表示するフィールドを選択する
    list_display = ["question_text", "pub_date", "was_published_recently"]

    # filter機能を付ける
    list_filter = ["pub_date"]

    # search機能を付ける
    search_fields = ["question_text"]
    fieldsets = [
        (None,                  {"fields": ["question_text"]}),
        # classes collapseはdivにcollapseクラスを設定して畳み込み可能にするため
        ("Date information",    {"fields": ["pub_date"], "classes":["collapse"]}),
    ]
    # inlineはクラスから継承したメンバ
    inlines = [ChoiceInline]

# tabular inlineを用いてすっきりとしたテーブルにする

class ChoiceTabularInline(admin.TabularInline):
    model = Choice
    extra = 3

class QuestionAdminWithTabularChoice(QuestionAdminWithChoice):
    inlines = [ChoiceTabularInline]

admin.site.register(Question, QuestionAdminWithTabularChoice)

# Choice（QuestionとChoiceを別々に登録する必要があり、不便マン）
# admin.site.register(Choice)
