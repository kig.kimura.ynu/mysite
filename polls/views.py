from django.shortcuts import Http404
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.views import generic

from .models import Question

# IndexViewで未来の日付を表示させないために時間を利用する
from django.utils import timezone

# generic view
# ListView -> オブジェクトのリストを抽象化している
# たくさんのオブジェクトを1つのページに表示したいときにはListView

# DetailView -> あるオブジェクトの詳細ページを抽象化している
# 1つのオブジェクトを取り出し，そのメンバを使って何かしたいときにはDetailView
class IndexView(generic.ListView):
    template_name ="polls/index.html"
    # 指定しないとcontext_object_listは"question_list"に設定される
    context_object_name = "latest_question_list"

    def get_queryset(self):
        """Return the last five published questions"""
        # 自動でcontextを作成するメソッド
        # filterで未来の日付を抜く
        return Question.objects.filter(pub_date__lte=timezone.now()).order_by("-pub_date")[:5]

# 汎用Viewはhtmlのオブジェクト渡しとレンダリングを簡単に行ってくれるもの
# htmlは自分で書く必要がある
class DetailView(generic.DetailView):
    model = Question
    template_name = "polls/detail.html"

    # おそらく継承メソッドで条件を追加している感じ？
    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now())

class ResultsView(generic.DetailView):
    model = Question
    template_name = "polls/results.html"

# non-generic routing
# 関数ごとにビューが作られる？
def index(request):
    latest_question_list = Question.objects.order_by("-pub_date")[:5]

    # get_templateは自動的にtemplateディレクトリから取得する
    """
    template = loader.get_template("polls/index.html")
    context = {
        "latest_question_list": latest_question_list,
    }
    return HttpResponse(template.render(context, request))
    """
    # contextはtemplate中の引数を渡す辞書オブジェクト
    context = {"latest_question_list": latest_question_list}
    return render(request, "polls/index.html", context)

# question_id引数はurl中の<int:question_id>から解釈される
def detail(request, question_id):

    # オブジェクトを取るか，なければ例外処理を行うのはメソッドで定義されている
    """
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    """
    question = get_object_or_404(Question, pk=question_id)
    return render(request, "polls/detail.html", {"question": question})

def results(request, question_id):
    # パーセント記法の他にいろいろあるかチェック{}記法があればいいね
    question = get_object_or_404(Question, pk=question_id)
    return render(request, "polls/results.html", {"question": question})

# この仕組みだと競合状態に脆弱性がある
def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        # requestのPOSTはname属性に準拠
        selected_choice = question.choice_set.get(pk=request.POST["choice"])
    except(KeyError, Choice.DoesNotExist):
        # エラー表示を行い，もう一度質問フォームを表示する
        return render(request, "polls/detail.html", {
            "question": question,
            "error_message": "Yout didn't select a choice",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the back button
        return HttpResponseRedirect(reverse("polls:results", args=(question.id, )))
