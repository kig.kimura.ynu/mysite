from django.urls import path

from . import views

# app_nameを付けることで名前空間を付けられる（同じurl名でも使える）
# app_nameをつけたら必ず名前空間を付けなければならない
app_name = 'polls'
# urlpatternsの名前は固定的
# generic view を使う
urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path("<int:pk>/", views.DetailView.as_view(), name="detail"),
    path("<int:pk>/results/", views.ResultsView.as_view(), name="results"),
    path("<int:question_id>/vote", views.vote, name="vote"),
]
# genericを使わない仕様
'''
urlpatterns = [
    # ""はpolls applicationのルート
    path("", views.index, name="index"),

    # name属性を付けるのはテンプレートタグ<% url %>を使うため
    # ex: /polls/5/ 詳細表示用のview
    path("<int:question_id>/", views.detail, name="detail"),

    # ex: /polls/5/results/ 結果表示用のview
    # <int:question_id>が位置引数でreverseのarg引数で渡される
    path("<int:question_id>/results/", views.results, name="results"),

    # ex: /polls/5/vote/ 投票用のview
    path("<int:question_id>/vote/", views.vote, name="vote"),

]
'''
